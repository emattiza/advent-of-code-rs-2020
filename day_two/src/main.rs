use std::fs::File;
use std::io::{BufReader, BufRead, Error};
use day_two::policy::{parse_policy, Policy};
use day_two::word_count::count_chars_in_segment;

fn main() {
    let input = File::open("input.txt").unwrap();
    let buf_reader = BufReader::new(input);
    let policies: Vec<Option<Policy>> = buf_reader.lines().map(|line| {
        let line_read: String = match line {
            Ok(line_inner) => {line_inner}
            Err(e) => {panic!("Error Found in reading from file: {}", e)}
        };
        let policy: Option<Policy> = match parse_policy(line_read.as_str()) {
            Ok((_, pol)) => {Some(pol)}
            Err(_) => {None}
        };
        policy
    }).collect();
    let results: Vec<usize> = policies.iter().map(|p| {
        let result: usize = match p {
            None => {0}
            Some(Policy {min, max, char, password }) => {
                let word_map = count_chars_in_segment(password.as_str());
                let count = word_map.get(&char).unwrap_or(&0);
                match (count >= min, count <= max) {
                    (true, true) => {1},
                    (_,_) => {0}
                }
            }
        };
        result
    }).collect();
    let answer_1: usize = results.iter().sum();
    println!("{:#?}", answer_1);
    let policies_2 = policies.clone();
    let answers: Vec<usize> = policies_2.iter().map(|p| {
        let answer: usize = match p {
            None => {0},
            Some(Policy { min, max, char, password }) => {
                let pwd = password.as_str();
                let c1 = pwd.chars().nth(*min - 1).unwrap_or('?');
                let c2 = pwd.chars().nth(*max - 1).unwrap_or('?');
                match (c1 == *char, c2 == *char) {
                    (true, true) => {0},
                    (true, _) => {1},
                    (_, true) => {1},
                    (_, _) => {0}
                }
            }
        };
        answer
    }).collect();
    println!("{:#?}", answers.iter().sum::<usize>())

}
