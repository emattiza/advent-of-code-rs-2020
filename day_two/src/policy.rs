use nom::{IResult, ParseTo};
use nom::sequence::{separated_pair};
use nom::character::complete::{digit1, char, space1, anychar, alpha1};
use nom::combinator::{map_opt};



#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Policy {
    pub min: usize,
    pub max: usize,
    pub char: char,
    pub password: String
}

impl Policy {
    pub fn check_valid(&mut self) {
        let pwd = self.password.as_str();

    }
}

pub fn parse_min_max(input: &str) -> IResult<&str, (usize, usize)> {
        let pair_parse = separated_pair(
            digit1,
            char('-'),
            digit1
        );
        map_opt(
            pair_parse,
                |s| {
                    let (s1, s2): (&str, &str) = s;
                    let answer: Option<(usize,usize)> = match (s1.parse_to(), s2.parse_to()) {
                        (Some(p), Some(q)) => Some((p, q)),
                        (_, _) => None
                    };
                    return answer;
            })
            (input)
}


pub fn parse_policy(input: &str) -> IResult<&str, Policy> {
    do_parse!(input,
        minmax: parse_min_max >>
        space1 >>
        char: anychar >>
        anychar >>
        space1 >>
        pass: alpha1 >>
        (Policy {min: minmax.0, max: minmax.1, char: char, password: pass.to_string()})
    )
}

#[test]
fn test_construct_min_max() {
    let input = "1-3";
    let answer: (usize, usize) = (1,3);
    assert_eq!(parse_min_max(input), Ok(("", answer)));
}

#[test]
fn test_construct_policy() {
    let input = "1-3 a: aaaaaaa";
    let answer = Ok(("", Policy {min: 1, max: 3, char: 'a', password: "aaaaaaa".to_string()}));
    assert_eq!(parse_policy(input), answer)
}