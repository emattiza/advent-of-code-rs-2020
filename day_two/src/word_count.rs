use std::collections::HashMap;
use std::str::Chars;

pub fn count_chars_in_segment(s: &str) -> HashMap<char, usize> {
    let chars: Chars = s.clone().chars();
    let mut map: HashMap<char, usize> = HashMap::new();
    for char in chars {
        let counter = map.entry(char).or_insert(0usize);
        *counter += 1;
    }
    map
}


macro_rules! test_map_create {
    ($input:expr, $expected:expr) => {
        let input = $input;
        let answer: HashMap<char, usize, RandomState> = count_chars_in_segment(input);
        let mut expected: HashMap<char, usize, RandomState> = HashMap::new();
        let expected_vec: Vec<(char, usize)> = $expected;
        expected_vec.iter().for_each(|p| {
            expected.insert(p.0, p.1);
        });
        assert_eq!(answer, expected);
    }
}

#[test]
fn test_word_counting() {
    test_map_create!("abcde", vec![('a', 1), ('b', 1), ('c', 1), ('d', 1), ('e', 1)]);
    test_map_create!("cdefg", vec![('c', 1), ('d', 1), ('e', 1), ('f', 1), ('g', 1)]);
    test_map_create!("ccccccccc", vec![('c', 9)]);
}

