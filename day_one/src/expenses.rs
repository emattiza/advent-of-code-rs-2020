use std::str::FromStr;
use std::num::ParseIntError;
use std::ops::{Mul, Add};

#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd)]
pub struct Expense {
    pub(crate) amt: isize
}

impl Expense {
    pub fn new() -> Expense {
        Expense { amt: 0 }
    }
}

impl FromStr for Expense {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.parse::<isize>() {
            Ok(amt) => {Ok(Expense { amt })}
            Err(e) => {Err(e)}
        }
    }
}

impl Mul for Expense {
    type Output = Expense;

    fn mul(self, rhs: Self) -> Self::Output {
        let amt = self.amt.mul(rhs.amt);
        Expense {amt}
    }
}

impl Add for Expense {
    type Output = Expense;

    fn add(self, rhs: Self) -> Self::Output {
        let amt = self.amt.add(rhs.amt);
        Expense {amt}
    }
}

