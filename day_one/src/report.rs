use crate::expenses::Expense;
use std::str::FromStr;
use std::num::ParseIntError;
use itertools::Itertools;

extern crate itertools;

#[derive(Debug, Clone)]
pub struct Report {
    expenses: Vec<Expense>
}

impl FromStr for Report {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let lines = s.split::<&str>("\n");
        let mut report = Report { expenses: Vec::new() };
        for line in lines {
            match line.parse::<Expense>().ok() {
                None => {}
                Some(expense) => {
                    report.expenses.push(expense);
                }
            }
        }
        Ok(report)
    }
}

pub fn find_matches_product(rep: Report) -> Vec<isize> {
    let pairs = rep.expenses.iter().combinations(2);

    pairs.filter_map(|mut pair| {
        let (x, y): (isize, isize) = (pair.pop().unwrap().amt, pair.pop().unwrap().amt);
        return if x + y == 2020 {
            Some(x * y)
        } else {
            None
        };
    }).collect_vec()
}


pub fn find_matches_triples_product(rep: Report) -> Vec<isize> {
    let triples = rep.expenses.iter().combinations(3);

    triples.filter_map(|mut pair| {
        let (x, y, z): (isize, isize, isize) = (
            pair.pop().unwrap().amt,
            pair.pop().unwrap().amt,
            pair.pop().unwrap().amt
        );
        return if x + y + z == 2020 {
            Some(x * y * z)
        } else {
            None
        };
    }).collect_vec()
}

#[test]
fn test_find_match_2020_pairs() {
    let input = "1721\n979\n366\n299\n675\n1456";
    let rep = input.parse::<Report>().unwrap();
    let out = find_matches_product(rep);
    println!("{:#?}", out);
    assert_eq!(out, vec![514579]);
}

#[test]
fn test_find_match_2020_triples() {
    let input = "1721\n979\n366\n299\n675\n1456";
    let rep = input.parse::<Report>().unwrap();
    let out = find_matches_triples_product(rep);
    println!("{:#?}", out);
    assert_eq!(out, vec![241861950]);
}


