use day_one::expenses::Expense;
use day_one::report::{Report, find_matches_product, find_matches_triples_product};
use std::fs::File;
use std::io::Read;

fn main() {
    let mut input = File::open("input.txt").unwrap();
    let mut contents: String = String::new();
    input.read_to_string(&mut contents).unwrap();
    let report = contents.as_str().parse::<Report>().unwrap();
    let report2 = report.clone();
    let answers = find_matches_product(report);
    println!("{:#?}", answers);
    let answers_2 = find_matches_triples_product(report2);
    println!("{:#?}", answers_2);
}
